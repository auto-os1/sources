/*
* This file was generated by the CommonAPI Generators.
* Used org.genivi.commonapi.core 3.2.14.v202310241605.
* Used org.franca.core 0.13.1.201807231814.
*
* This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0.
* If a copy of the MPL was not distributed with this file, You can obtain one at
* http://mozilla.org/MPL/2.0/.
*/
#ifndef V1_COMMONAPI_HELLO_WORLD_INSTANCE_HPP_
#define V1_COMMONAPI_HELLO_WORLD_INSTANCE_HPP_

#include <string>

namespace v1 {
namespace commonapi {

const char * const HelloWorld_test = "test";

const std::string HelloWorld_INSTANCES[] = {
    HelloWorld_test
};

} // namespace commonapi
} // namespace v1

// Compatibility
namespace v1_0 = v1;

#endif // V1_COMMONAPI_HELLO_WORLD_INSTANCE_HPP_
